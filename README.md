
 # projet Cordova

Sylvain Dupuy
Tony Duong

 ## Ajouter les platforms et plugins
 
 ```bash
 cordova prepare
 ```

# correction car le plugin mapbox a un mauvais minSdk

dans `plugin/cordova-plugin-mapbox/src/android/mapbox.gradle` :

changer

```bash
 ext.cdvMinSdkVersion = 15
```
en
 ```bash
 ext.cdvMinSdkVersion = 19
  ```

Puis :

 ```bash
 cordova platform remove android
 cordova platform add android
 ```

# Lancer l'appli !

 ```bash
 cordova run android
 ```