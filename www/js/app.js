const TYPES = {
    TEXT: 't',
    PHOTO: 'p',
    VIDEO: 'v',
    GEO: 'g'
}

class Note {
    constructor(title) {
        this.title = title;
        this.elements = new Array();
    }
}

class Element {
    constructor(content, type) {
        this.type = type;
        this.content = content;
    }
}