function showNote(i) {
  let container = document.getElementById('noteContainer');
  let note = JSON.parse(localStorage.getItem(i));

  // On met le titre
  let title = document.getElementById("titleNote");
  title.innerText = note.title

  // On affiche tout les éléments de la note
  note.elements.forEach(element => {
    switch (element.type) {
      case TYPES.TEXT:
        container.append(document.createTextNode(element.content));
        break;
      case TYPES.VIDEO:
          var v = "<video id='myVideo' style='max-height: 500px;' controls='controls'>";
          v += "<source src='" + element.content + "' type='video/mp4'>";
          v += "</video>";
          var div = document.createElement('div');
          div.innerHTML = v;
          container.append(div.firstChild);
        break;
      case TYPES.PHOTO:
        var newImg = document.createElement("img")
        newImg.setAttribute("src", element.content)
        newImg.style.maxWidth = '50vw';
        newImg.style.maxHeight = '50vh';
        container.append(newImg)
        break;
      case TYPES.GEO:
        var latitude = element.content.lat;
        var longitude = element.content.long;
        if (device.platform !== 'browser') {
          Mapbox.show({
              style: 'emerald',
              margins: {
                  'left': 0,
                  'right': 0,
                  'top': (window.screen.height/3)*2,
                  'bottom': 0
              },
              center: {
                  lat: latitude,
                  lng: longitude
              },
              markers: [{
                  'lat': latitude,
                  'lng': longitude,
                  'title': 'Your location',
                  'subtitle': 'Latitude: ' + latitude + ', ' +
                      'Longitude: ' + longitude,
              }]
          });
        } else {
            let text = document.getElementById('browserGeo');
            text.innerText = 'Latitude: ' + latitude + ', ' +
                'Longitude: ' + longitude;
        }
        break;
      default:
        break;
    }
    container.append(document.createElement("br"));
  });
}

var app = {
  // Application Constructor
  initialize: function () {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
  },

  // deviceready Event Handler
  //
  // Bind any cordova events here. Common events are:
  // 'pause', 'resume', etc.
  onDeviceReady: function () {
    navigator.splashscreen.show();
    setTimeout(function () {
      navigator.splashscreen.hide();
    }, 1000);
    showNote(location.search.split('noteIndex=')[1]);
  },
};

app.initialize();