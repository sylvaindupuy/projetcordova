function openNav() {
    var x = document.getElementById("myTopNav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

window.onload = function () {
    document.getElementById("btnBurger").addEventListener("click", openNav);
}