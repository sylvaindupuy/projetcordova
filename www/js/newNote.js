let latitude = null, longitude = null;


function goToIndex(){
    window.location = 'index.html';
    Mapbox.hide();
}

function addNoteForm(event) {
    var nb = localStorage.length + 1;
    var title = document.getElementById('noteTitle').value;

    if(title != ''){
        var note = new Note(title);

        // Description
        if(document.getElementById('noteContent').value != null){
            var content = document.getElementById('noteContent').value;
            var element = new Element(content, TYPES.TEXT);
            note.elements.push(element);
        }

        // Image
        if(document.getElementById('myImage') != null){
            var photo = document.getElementById('myImage').src;
            var photoelement = new Element(photo, TYPES.PHOTO);
            note.elements.push(photoelement);
        }

        // Video
        if(document.getElementById('myVideo') != null){
            var video = document.getElementById('myVideo').firstChild.src;
            var videoElement = new Element(video, TYPES.VIDEO);
            note.elements.push(videoElement);
        }

        // Geo localisation
        if(latitude != null && longitude != null){
            var position = {lat: latitude, long: longitude};
            var locationElement = new Element(position, TYPES.GEO);
            note.elements.push(locationElement);
        }

        localStorage.setItem(nb, JSON.stringify(note));

        event.preventDefault();
        goToIndex();
    }
}


function cameraTakePicture(e) {
    var fromFile = e.target.id == "addPicture"
    if (fromFile) {
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        });
    } else {
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
        });
    }
    
    function onSuccess(imageData) {
        var v = "<img id='myImage' style='max-width: 50vw;max-height: 50vh'";
        v+= " src='data:image/jpeg;base64," + imageData + "'></img>";
        v += "</img>";
        document.querySelector("#photoArea").innerHTML = v;
    }
    function onFail(message) {
        alert('Failed because: ' + message);
    }
}

function cameraTakeVideo(e) {
    navigator.device.capture.captureVideo(onSuccess, onFail);
    function onSuccess(s) {
        var v = "<video id='myVideo' style='max-height: 500px;' controls='controls'>";
        v += "<source src='" + s[0].fullPath + "' type='video/mp4'>";
        v += "</video>";
        document.querySelector("#videoArea").innerHTML = v;
    }
    function onFail(message) {
        alert('Failed because: ' + message);
    }
}

function addGeolocation(event) {
    navigator.geolocation.getCurrentPosition(onSuccess, onError, {
        enableHighAccuracy: true
    });

    event.preventDefault();

    function onError(error) {
        alert('erreur lors de la recuperation de votre position.' + '\n' + 'code: ' + error.code + '\n' +
            'message: ' + error.message + '\n');
    }
    
    function onSuccess(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        if (device.platform !== 'browser') {
            Mapbox.show({
                style: 'emerald',
                margins: {
                    'left': 0,
                    'right': 0,
                    'top': (window.screen.height/3)*2,
                    'bottom': 0
                },
                center: {
                    lat: latitude,
                    lng: longitude
                },
                markers: [{
                    'lat': latitude,
                    'lng': longitude,
                    'title': 'Your location',
                    'subtitle': 'Latitude: ' + latitude + ', ' +
                        'Longitude: ' + longitude,
                }]
            });
        } else {
            let text = document.getElementById('browserGeo');
            text.innerText = 'Latitude: ' + latitude + ', ' +
                'Longitude: ' + longitude;
        }
    }
}


var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        Mapbox.hide();
        navigator.splashscreen.show();
        setTimeout(function () {
            navigator.splashscreen.hide();
        }, 1000);
        document.getElementById('submitBtn').addEventListener('click', addNoteForm);
        document.getElementById("addPicture").addEventListener("click", cameraTakePicture);
        document.getElementById("addVideo").addEventListener("click", cameraTakeVideo);
        document.getElementById("cameraTakePicture").addEventListener("click", cameraTakePicture);
        document.getElementById('geolocation').addEventListener('click', addGeolocation);
    },
};

app.initialize();